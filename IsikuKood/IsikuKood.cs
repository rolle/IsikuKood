﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IsikuKood
{
    public static class IsikuKood
    // isikukoodi pillerkaar
    {
        static Random r = new Random();
        static bool trueFalse() => r.Next() % 2 == 0;
        // idee www.dotnetperls.com/datetime
        private static DateTime getNewDateBetween(DateTime startDate, DateTime endDate)
        {
            TimeSpan timeSpan = endDate - startDate;
            TimeSpan newSpan = new TimeSpan(r.Next(0, (int)timeSpan.TotalHours), 0, 0); //argumentideks H, M, S
            return startDate + newSpan;
        }
        // kas isikukoodis olev kuupäev on üldse olemas? N:29.02.00 vs 29.02.01
        private static bool isValidDateTime(string isikukood)
        {
            DateTime dt = new DateTime();
            if (DateTime.TryParse(
                $"{isikukood.Substring(5, 2)}." +
                $"{isikukood.Substring(3, 2)}." +
                $"{GetBirthYear(isikukood)}",
                out dt)
                ) return true;
            else return false;
        }
        // leia isikukoodi kontroll(viimane)number wikipedia isikukoodi artikli põhjal
        private static int getIKControlNumber(string kood)
        {
            int[] ikood = new int[10];
            for (int i = 0; i < 10; i++)
            {
                ikood[i] = kood[i] - '0';
            }
            // esimese astme kaal : 1 2 3 4 5 6 7 8 9 1
            int kaal = 1 * ikood[0]
                     + 2 * ikood[1]
                     + 3 * ikood[2]
                     + 4 * ikood[3]
                     + 5 * ikood[4]
                     + 6 * ikood[5]
                     + 7 * ikood[6]
                     + 8 * ikood[7]
                     + 9 * ikood[8]
                     + 1 * ikood[9];
            // Kui jagatise jääk ei võrdu 10ga, on jääk kontrollnumbriks.
            if ((kaal % 11) < 10)
            {
                return kaal % 11;
            }
            else
            {
                // Kui jääk võrdub 10ga, siis korrutatakse koodi kümme esimest numbrit igaüks omaette II astme kaaluga.Korrutised liidetakse.
                // II astme kaal: 3 4 5 6 7 8 9 1 2 3
                kaal = 3 * ikood[0]
                     + 4 * ikood[1]
                     + 5 * ikood[2]
                     + 6 * ikood[3]
                     + 7 * ikood[4]
                     + 8 * ikood[5]
                     + 9 * ikood[6]
                     + 1 * ikood[7]
                     + 2 * ikood[8]
                     + 3 * ikood[9];
                // Saadud summa jagatakse 11ga.Kui jääk ei võrdu 10ga, on saadud jääk kontrollnumbriks. 
                // Kui jääk võrdub 10ga, siis on kontrollnumber 0.
                return (kaal % 11) % 10;
            }
        }

        // tagasta sellele isikukoodile vastav vanus
        public static int GetAge(string isikukood)
        {
            DateTime skp = GetBirthDate(isikukood);
            int d = DateTime.Now.Year - skp.Year;
            if (skp.AddYears(d) > DateTime.Now.Date) d--;
            return d;
        }
        public static int Age(this string isikukood)
        {
            DateTime skp = GetBirthDate(isikukood);
            int d = DateTime.Now.Year - skp.Year;
            if (skp.AddYears(d) > DateTime.Now.Date) d--;
            return d;
        }

        // kas see isikukood on Woman (2, 4, 6, ...) oma
        public static bool IsWoman(string kood) => (kood[0] % 2) == 0;
        public static bool IsWoman(char kood) => (kood % 2) == 0;
        public static bool IsWoman(int kood) => (kood % 2) == 0;

        // tagasta sellele isikukoodile vastav sünniaasta
        public static int GetBirthYear(string isikukood) //hennu loogika
           => 1800 + ((isikukood[0] - '1') / 2) * 100 + //1800 | 1900 | 2000
              int.Parse(isikukood.Substring(1, 2));
        public static int BirthYear(this string isikukood) //hennu loogika
           => 1800 + ((isikukood[0] - '1') / 2) * 100 + //1800 | 1900 | 2000
              int.Parse(isikukood.Substring(1, 2));

        // tagasta sellele isikukoodile vastav sündimise kuupäev
        public static DateTime GetBirthDate(string isikukood) // hennu loogika
            => new DateTime( //tekitame tema jaoks kolm int-i AAAA, KK, PP
            GetBirthYear(isikukood),
            int.Parse(isikukood.Substring(3, 2)),
            int.Parse(isikukood.Substring(5, 2)));
        public static DateTime BirthDate(this string isikukood) // hennu loogika
            => new DateTime( //tekitame tema jaoks kolm int-i AAAA, KK, PP
            GetBirthYear(isikukood),
            int.Parse(isikukood.Substring(3, 2)),
            int.Parse(isikukood.Substring(5, 2)));
         
        //isikukoodi vormiline kontroll
        public static bool IsValidIsikukood(string isikukood)
        {
            // olgu 11 symbolit
            if (isikukood.Length != 11) return false;

            // symboliteks olgu ainult numbrid
            try
            {
                long.Parse(isikukood); //stringist
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            // seni jagatud vaid 1..6-ga algavaid koode
            if ((isikukood[0] - '0') < 1 || (isikukood[0] - '0') > 6) return false;

            // kas selline isikukoodis peituv synnikuupäev reaalselt eksisteerib
            if (!isValidDateTime(isikukood)) return false;

            // kas kontrollkood on õige
            if (getIKControlNumber(isikukood) != (isikukood[10] - '0')) return false;

            //saaks veel juurde toppida
            // haigla koodi kontrolli (ennem 2013 sõundinutel)
            // samal päeval sõndinute järjekorranumbri reaalsuse kontrolli (vaevalt 999 last ühes kuupäevas) 

            return true;
        } // public static bool ValidateIsikukood (string isikukood)

        // otsime juhuslikku isikukoodilaadset asja 
        public static string GetNewPseudoIK(bool isMan, DateTime birthDate)
        {
            string uusIK = "";
            //esimene symbol
            uusIK = (birthDate.Year / 100 * 2 - 34 - Convert.ToInt16(isMan)).ToString();
            // aasta, kuu, päev
            uusIK += (birthDate.Year % 100).ToString("D2") + birthDate.Month.ToString("D2") + birthDate.Day.ToString("D2");
            //haigla, järjekorranumber
            uusIK += r.Next(1, 999).ToString("D3");
            // ja kontrollnumber
            uusIK += getIKControlNumber(uusIK);
            return uusIK;
        } // public static string GetNewPseudoIK(int Sugu, DateTime birthDate)

        public static string GetNewPseudoIK()
        {
            //küsime funktsioonilt uue juhusliku synnikuupäeva
            DateTime endDate = DateTime.Now; // tulevikuinimesi ei tee
            DateTime startDate = endDate.AddYears(-100); // ärme väga vanu inimesi tee
            DateTime synniKuup = getNewDateBetween(startDate, endDate);
            //paneme paika soo (GetNewPseudoIK(bool isMan, DateTime birthDate) - false-naine true-mees
            return GetNewPseudoIK(trueFalse(), synniKuup);
        } 
        public static string GetNewPseudoIK(int BirthYear)
        {
            DateTime endDate = new DateTime(BirthYear + 1, 1, 1); // järgmise aasta 1. jaanuar
            DateTime startDate = new DateTime(BirthYear, 1, 1); 
            return GetNewPseudoIK(trueFalse(), getNewDateBetween(startDate, endDate));
        }
        public static string GetNewPseudoIK(int StartBirthYear, int EndBirthYear)
        {
            if (StartBirthYear < 1801 || EndBirthYear > 2199) return "";
            DateTime endDate = new DateTime(EndBirthYear + 1, 1, 1);
            DateTime startDate = new DateTime(StartBirthYear, 1, 1);
            return GetNewPseudoIK(trueFalse(), getNewDateBetween(startDate, endDate));
        }

        public static string GetNewWomanPseudoIK()
        {
            DateTime endDate = DateTime.Now;
            DateTime startDate = endDate.AddYears(-100);
            return GetNewPseudoIK(false, getNewDateBetween(startDate, endDate));
        }
        public static string GetNewWomanPseudoIK(int BirthYear)
        {
            DateTime endDate = new DateTime(BirthYear + 1, 1, 1);
            DateTime startDate = new DateTime(BirthYear, 1, 1);
            return GetNewPseudoIK(false, getNewDateBetween(startDate, endDate));
        }
        public static string GetNewWomanPseudoIK(int StartBirthYear, int EndBirthYear)
        {
            if (StartBirthYear < 1801 || EndBirthYear > 2199) return "";
            DateTime endDate = new DateTime(EndBirthYear + 1, 1, 1);
            DateTime startDate = new DateTime(StartBirthYear, 1, 1);
            return GetNewPseudoIK(false, getNewDateBetween(startDate, endDate));
        }

        public static string GetNewManPseudoIK()
        {
            DateTime endDate = DateTime.Now;
            DateTime startDate = endDate.AddYears(-100);
            return GetNewPseudoIK(true, getNewDateBetween(startDate, endDate));
        }
        public static string GetNewManPseudoIK(int BirthYear)
        {
            DateTime endDate = new DateTime(BirthYear + 1, 1, 1);
            DateTime startDate = new DateTime(BirthYear, 1, 1);
            return GetNewPseudoIK(true, getNewDateBetween(startDate, endDate));
        }
        public static string GetNewManPseudoIK(int StartBirthYear, int EndBirthYear)
        {
            if (StartBirthYear < 1801 || EndBirthYear > 2199) return "";
            DateTime endDate = new DateTime(EndBirthYear + 1, 1, 1);
            DateTime startDate = new DateTime(StartBirthYear, 1, 1);
            return GetNewPseudoIK(true, getNewDateBetween(startDate, endDate));
        }
    }
}
